exports.LIMIT_RECENT_CLASSES = 5;
exports.LIMIT_PER_PAGE = 50;
exports.DEBUG = false;

exports.TYPE_PAY_BEFORE = 1;
exports.TYPE_PAY_AFTER = 2;