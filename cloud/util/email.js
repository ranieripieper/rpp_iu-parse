var _ = require('underscore');

var Class = Parse.Object.extend('Class');
var Teacher = Parse.Object.extend('_User');
var Student = Parse.Object.extend('Student');
var Constants = require('cloud/util/constants.js');


exports.send_welcome_teacher = function(teacher) {
	promises = []
	var Mailgun = require('mailgun');
    Mailgun.initialize('iuapp.co', 'key-8e3514dcb074e34cf0f20bc58ea3c2df');

    var name = get_name_assinatura();

    promises.push(
	    Mailgun.sendEmail({
	      to: teacher.get("email"),
	      from: name + " - info us<contato@iuapp.co>",
	      subject: "Eba, bem-vindo!",
	      html: email_welcome_teacher(teacher.get("name"), teacher.get("email"), get_name_assinatura_email(name))
	    }, {
	      success: function(httpResponse) {
	      	teacher.set("sentWelcomeMail", true);
	      	teacher.save();
	        console.log("Email sent! " + teacher.get("email"));
	      },
	      error: function(httpResponse) {
	        console.log("Email NOT sent! " + teacher.get("email"));
	      }
	    }));

    return Parse.Promise.when(promises);
}

var get_name_assinatura = function() {
	var names = ["Gil", "Rafa", "Pri", "Rani"];
	var number = Math.floor(Math.random() * (names.length));
	return names[number];
}

var get_name_assinatura_email = function(name) {
	return "<b>" + name + "</b> | CEO info us";
}


var email_welcome_teacher = function(name, email, name_assinatura) {
	return "<!DOCTYPE>" +
			"<html xmlns='http://www.w3.org/1999/xhtml'>" +
			"	<head>" +
			"		<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />" +
			"		<title>info us.</title>" +
			"		<meta name='viewport' content='width=device-width, initial-scale=1.0'/>" +
			"	</head>" +
			"</html>" +

			"<body style='margin: 0; padding: 0;color: #ACACAC; font-family: Avenir, sans-serif; font-size: 14px; '>" +
			"	<table align='center' border='0' cellpadding='0' cellspacing='0' width='600' style='border: 1px solid #f6f6f6;'>" +
			"		<tr>" +
			"			<td align='' bgcolor='#f6f6f6' style='padding: 20px 0 20px 20px;'><img src='http://iuapp.co/img/img_logo_small.png' alt='info us' style='display: block;' /></td>" +
			"		</tr>" +
			"		<tr>" +
			"			<td bgcolor='#ffffff' style='padding: 40px 30px 40px 30px;'>&nbsp;</td>" +
			"		</tr>		" +
			"		<tr>" +
			"			<td style='color: #ACACAC; font-family: Avenir, sans-serif; font-size: 14px; line-height: 20px;'>" +
			"				<p style='padding-left:20px; padding-right:20px'>" +
			"					Oi " + name + "!" +
			"				</p>" +
			"				<p style='padding-left:20px; padding-right:20px'>" +
			"					Antes de tudo, bem-vindo ao info us e obrigado por usar o nosso aplicativo." +
			"				</p>" +
			"				<p style='padding-left:20px; padding-right:20px'>" +
			"					Ele foi todo desenvolvido pensando em ajudar na organização da sua vida profissional!" +
			"				</p>" +
			"				<p style='padding-left:20px; padding-right:20px'>" +
			"					Pode ter certeza que, a partir de agora, vai ficar bem mais fácil controlar as aulas e os pagamentos dos seus alunos." +
			"				</p>" +
			"				<p style='padding-left:20px; padding-right:20px'>" +
			"					Se você tiver qualquer dúvida, conta comigo! :)" +
			"				</p>" +
			"				<p style='padding-left:20px; padding-right:20px'>" +
			"					E, mais uma vez, bem-vindo ao info us!" +
			"				</p>" +
			"			</td>" +
			"		</tr>" +
			"		<tr>" +
			"			<td style='font-size: 0; line-height: 0;' height='10'>&nbsp;</td>" +
			"		</tr>" +
			"		<tr>" +
			"			<td style='font-size: 0; line-height: 0;' height='10'>&nbsp;</td>" +
			"		</tr>" +
			"		<tr>" +
			"			<td>" +
			"				<table>" +
			"					<tr>" +
			"						<td style='font-size: 0; line-height: 0;' height='10'>&nbsp;</td>" +
			"					</tr>" +
			"					<tr>" +
			"						<td style='color: #ACACAC; font-family: Avenir, sans-serif; font-size: 14px; line-height: 20px;'> " +
			"							<p style='padding-left:20px; padding-right:20px'><a href='mailto:contato@iuapp.co' style='text-decoration:none; color: #ACACAC; font-family: Avenir, sans-serif; font-size: 14px; line-height: 20px;'>"+ name_assinatura + "</a></p>" +
			"						</td>" +
			"					</tr>" +
			"					<tr><td style='font-size: 0; line-height: 0;' height='30'>&nbsp;</td></tr>" +
			"				</table>" +
			"			</td>" +
			"		</tr>" +
			"		<tr>" +
			"			<td align='center' bgcolor='#252830' style= 'background:#282828; padding:20px; text-align: center; border-color: #1D1F21 font-size: 8px;' ><a href='https://www.facebook.com/iuapp.co/' target='blank'><img src='http://iuapp.co/img/icn_fb.png' alt='facebook' style='display: inline; padding: 5px;'/></a><a href='https://www.instagram.com/iu.app/' target='blank'><img src='http://iuapp.co/img/icn_insta.png' alt='instagram' style='display: inline; padding: 5px;'/></a><a href='mailto:contato@iuapp.co'><img src='http://iuapp.co/img/icn_mail.png' alt='email' style='display: inline; padding: 5px;'/></a>" +
			"			<br style='color:white; line-height: 30px; text-decoration: none;'>" +
			"			<a target= 'blank' href='http://iuapp.co' align='right' bgcolor='' style='color: #fff; font-family: Avenir, sans-serif; font-size: 14px; text-decoration: none; padding: 10px;'>www.iuapp.co</a></td>" +
			"		</tr>" +
			"		<tr>" +
			"			<td align='center'  style='color: #b7b7b7; font-family: Avenir, sans-serif; font-size: 10px; line-height: 20px; background:#2b2b2b; padding: 5px;'> Copyright © 2016 info us. Todos os direitos reservados. </td>" +
			"		</tr>" +
			"	</table>" +
			"</body>" +
			"</html>";
}