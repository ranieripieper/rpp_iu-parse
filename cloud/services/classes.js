var _ = require('underscore');

var Class = Parse.Object.extend('Class');
var Teacher = Parse.Object.extend('_User');
var Student = Parse.Object.extend('Student');
var Constants = require('cloud/util/constants.js');

exports.get_done = function(phoneNumber, objectIdTeacher, page) {

    if (Constants.DEBUG) {
        console.log('student_get_classes >>> ' + 'phoneNumber: ' + phoneNumber + ' objectIdTeacher: ' + objectIdTeacher
             + ' page: ' + page);
    }

    if (page == null || page == undefined || page == 0) {
        page = 1;
    }
    var resultsJson = null;

    var studentQuery = new Parse.Query(Student);
    studentQuery.equalTo("phone", parseInt(phoneNumber));

    var teacher = new Teacher();
    teacher.id = objectIdTeacher;

    var query = new Parse.Query(Class);
    query.select('objectId', 'done', 'usedAt', 'expiredAt', 'usedAt', 'student');
    query.equalTo("done", true);
    query.exists("usedAt");
    query.exists("student");
    query.limit(Constants.LIMIT_PER_PAGE);
    query.skip((parseInt(page)-1)*Constants.LIMIT_PER_PAGE);
    query.containedIn("instructor", [teacher]);
    query.matchesQuery("student", studentQuery);
    query.ascending("usedAt");

    return query.find(function(results) {
        if (Constants.DEBUG && results != null) {
            console.log('>>> nr recent classes: ' + results.length);
        }       
        recentClasses = results;
        return results;
    });
};

//Serviços para = PAGA ANTES
exports.pay_before_add_classes = function(objectIdStudent, objectIdTeacher, qtClasses, dtValidade) {

    if (Constants.DEBUG) {
        console.log('before_paid_add_classes >>> ' + 'objectIdStudent: ' + objectIdStudent + ' objectIdTeacher: ' + objectIdTeacher
             + ' qtClasses: ' + qtClasses + ' dtValidade: ' + dtValidade + ' Date: ' + new Date());
    }

    var student = new Student();
    student.id = objectIdStudent;

    var teacher = new Teacher();
    teacher.id = objectIdTeacher;

    var date = new Date(dtValidade);
    date.setMinutes(0);
    date.setHours(0);
    date.setSeconds(0);
    date.setMilliseconds(0);
    promises = [];

    var campareDate = new Date();
    campareDate.setMinutes(0);
    campareDate.setHours(0);
    campareDate.setSeconds(0);
    campareDate.setMilliseconds(0);

    var query = new Parse.Query(Class);
    query.equalTo("done", false);
    query.notEqualTo("paid", false);
    query.greaterThanOrEqualTo( "expiredAt", campareDate);
    query.doesNotExist("usedAt");
    query.exists("student");
    query.containedIn("instructor", [teacher]);
    query.equalTo("student", student);
    
    promises = [];

    return query.find(function(results) {
        return results;
    }).then(function(results) {
        updatePromises = [];
        var clazzArray = [];
        if (Constants.DEBUG && results != null) {
            console.log('>>> before_paid_add_classes_nr_update: ' + results.length);
        }  else if (Constants.DEBUG) {
            console.log('>>> before_paid_add_classes_nr_update: 0');
        }
        _.each(results, function(result) {
            console.log('>>> before_paid_add_classes_save: ' + result.id);
            result.set("expiredAt", date);
            clazzArray.push(result);
        })

        updatePromises.push(Parse.Object.saveAll(clazzArray, {
            success: function(objs) {
                return objs;
            },
            error: function(error) { 
                throw error;
            }
        }));

        return Parse.Promise.when(updatePromises);
    }).then(function(results) {
        var clazzArray = [];
        for (var i = 0; i < parseInt(qtClasses); i++) {
            var clazz = new Class();
            clazz.set("student", student);
            clazz.relation("instructor").add(teacher);
            clazz.set("paid", true);
            clazz.set("usedAt", undefined);
            clazz.set("payAt", undefined);
            clazz.set("done", false);
            clazz.set("expiredAt", date);
            clazzArray.push(clazz);
        }
        promises.push(Parse.Object.saveAll(clazzArray, {
            success: function(objs) {
                return objs;
            },
            error: function(error) { 
                throw error;
            }
        }));
        return results;
    }).then(function(results) {
        return Parse.Promise.when(promises);
    });
};

exports.pay_before_set_done = function(objectIdStudent, objectIdTeacher, expiredAt, dates) {
    if (Constants.DEBUG) {
        console.log('before_paid_set_done >>> ' + 'objectIdStudent: ' + objectIdStudent + ' objectIdTeacher: ' + objectIdTeacher
             + ' expiredAt: ' + expiredAt + ' dates: ' + dates);
    }
    
    var student = new Student();
    student.id = objectIdStudent;

    var teacher = new Teacher();
    teacher.id = objectIdTeacher;

    var campareDate = new Date();
    campareDate.setMinutes(0);
    campareDate.setHours(0);
    campareDate.setSeconds(0);
    campareDate.setMilliseconds(0);

    var query = new Parse.Query(Class);
    query.equalTo("done", false);
    query.notEqualTo("paid", false);
    query.greaterThanOrEqualTo( "expiredAt", campareDate);
    query.doesNotExist("usedAt");
    query.exists("student");
    query.limit(dates.length);
    query.containedIn("instructor", [teacher]);
    query.equalTo("student", student);

    promises = []

    return query.find(function(results) {
        return results;
    }).then(function(results) {
        if (parseInt(results.length) != parseInt(dates.length)) {
            throw "Número de aulas compradas diferentes da quantidade de datas passadas";
        } else {
            var clazzArray = [];
            for (var i =0; i < results.length; i++) {
                var clazz = results[i];
                var date = new Date(dates[i]);
                clazz.set("done", true);
                clazz.set("usedAt", date);
                clazzArray.push(clazz);
            }
            promises.push(Parse.Object.saveAll(clazzArray, {
                success: function(objs) {
                    return objs;
                },
                error: function(error) { 
                    throw error;
                }
            }));

            if (expiredAt != null && expiredAt != "") {
                var dtExpiredAt = new Date(expiredAt);
                promises.push(updateExpiredAt(objectIdStudent, objectIdTeacher, dtExpiredAt));
            }
            return Parse.Promise.when(promises);
        }
    });
};

//Serviços para = PAGA DEPOIS
exports.pay_after_add_classes = function(objectIdStudent, objectIdTeacher, dtPaymentStr, dates) {

    if (Constants.DEBUG) {
        console.log('pay_after_add_classes >>> ' + 'objectIdStudent: ' + objectIdStudent + ' objectIdTeacher: ' + objectIdTeacher
             + ' dtPaymentStr: ' + dtPaymentStr + ' dates: ' + dates);
    }

    var student = new Student();
    student.id = objectIdStudent;

    var teacher = new Teacher();
    teacher.id = objectIdTeacher;

    promises = [];

    var dtPayment = new Date(dtPaymentStr);
    var clazzArray = [];
    for (var i = 0; i < parseInt(dates.length); i++) {
        var clazz = new Class();
        clazz.set("student", student);
        clazz.relation("instructor").add(teacher);
        clazz.set("paid", false);
        clazz.set("done", true);
        clazz.set("usedAt", new Date(dates[i]));
        clazz.set("payAt", dtPayment);
        clazz.set("expiredAt", undefined);
        clazzArray.push(clazz);
    }

    promises.push(Parse.Object.saveAll(clazzArray, {
        success: function(objs) {
            return objs;
        },
        error: function(error) { 
            throw error;
        }
    }));

    promises.push(updateDtPayment(objectIdStudent, objectIdTeacher, dtPayment));

    return Parse.Promise.when(promises);
};

exports.pay_after_update_to_paid = function(objectIdStudent, objectIdTeacher, qtClasses, dtPaymentStr) {
    if (Constants.DEBUG) {
        console.log('before_paid_set_done >>> ' + 'objectIdStudent: ' + objectIdStudent + ' objectIdTeacher: ' + objectIdTeacher
             + ' qtClasses: ' + qtClasses + ' dtPaymentStr: ' + dtPaymentStr);
    }
    var dtPayment = new Date(dtPaymentStr);
    var student = new Student();
    student.id = objectIdStudent;

    var teacher = new Teacher();
    teacher.id = objectIdTeacher;

    var query = new Parse.Query(Class);
    query.equalTo("done", true);
    query.equalTo("paid", false);
    query.exists("usedAt");
    query.exists("student");
    query.limit(parseInt(qtClasses));
    query.containedIn("instructor", [teacher]);
    query.equalTo("student", student);

    promises = [];

    return query.find(function(results) {
        return results;
    }).then(function(results) {
        if (parseInt(results.length) != parseInt(qtClasses)) {
            throw "Número de aulas feitas diferentes da quantidade de aulas pagas";
        } else {
            var clazzArray = [];
            promises.push(updateDtPayment(objectIdStudent, objectIdTeacher, dtPayment));
            for (var i =0; i < results.length; i++) {
                var clazz = results[i];
                clazz.set("paid", true);
                clazzArray.push(clazz);
            }
            promises.push(Parse.Object.saveAll(clazzArray, {
                success: function(objs) {
                    return objs;
                },
                error: function(error) { 
                    throw error;
                }
            }));

            return Parse.Promise.when(promises);
        }
    });
};

exports.pay_after_update_dt_pay = function(objectIdStudent, objectIdTeacher, dtPaymentStr) {
    if (Constants.DEBUG) {
        console.log('pay_after_update_dt_pay >>> ' + 'objectIdStudent: ' + objectIdStudent + ' objectIdTeacher: ' + objectIdTeacher
             + ' dtPaymentStr: ' + dtPaymentStr);
    }
    var dtPayment = new Date(dtPaymentStr);

    promises = [];
    promises.push(updateDtPayment(objectIdStudent, objectIdTeacher, dtPayment));
    return Parse.Promise.when(promises); 
}

var updateDtPayment = function(objectIdStudent, objectIdTeacher, dtPayment) {

    var student = new Student();
    student.id = objectIdStudent;

    var teacher = new Teacher();
    teacher.id = objectIdTeacher;

    var query = new Parse.Query(Class);
    query.equalTo("done", true);
    query.equalTo("paid", false);
    query.exists("usedAt");
    query.exists("student");
    query.containedIn("instructor", [teacher]);
    query.equalTo("student", student);

    updatePromises = [];

    return query.find(function(results) {
        return results;
    }).then(function(results) {
        if (Constants.DEBUG && results != null) {
            console.log('>>> updateDtPayment: ' + results.length);
        }  else if (Constants.DEBUG) {
            console.log('>>> updateDtPayment: 0');
        }
        if (results == null || results == undefined) {
            return null;
        }
        var clazzArray = []; 
        _.each(results, function(result) {
            console.log('>>> updateDtPayment: ' + result.id);
            result.set("payAt", dtPayment);
            clazzArray.push(result);
        })

        promises.push(Parse.Object.saveAll(clazzArray, {
            success: function(objs) {
                return objs;
            },
            error: function(error) { 
                throw error;
            }
        }));

        return Parse.Promise.when(updatePromises);
    });
}

var updateExpiredAt = function(objectIdStudent, objectIdTeacher, dtExpiredAt) {
 
    var student = new Student();
    student.id = objectIdStudent;

    var teacher = new Teacher();
    teacher.id = objectIdTeacher;

    var todayDate = new Date();
    todayDate.setMinutes(0);
    todayDate.setHours(0);
    todayDate.setSeconds(0);
    todayDate.setMilliseconds(0);

    var query = new Parse.Query(Class);
    query.equalTo("done", false);
    query.notEqualTo("paid", false);
    query.exists("student");
    query.containedIn("instructor", [teacher]);
    query.equalTo("student", student);
    query.greaterThanOrEqualTo("expiredAt", todayDate);

    updatePromises = [];

    return query.find(function(results) {
        return results;
    }).then(function(results) {
        if (Constants.DEBUG && results != null) {
            console.log('>>> updateExpiredAt: ' + results.length);
        }  else if (Constants.DEBUG) {
            console.log('>>> updateExpiredAt: 0');
        }
        if (results == null || results == undefined) {
            return null;
        }
        var clazzArray = []; 
        _.each(results, function(result) {
            if (Constants.DEBUG) {
                console.log('>>> updateExpiredAt: ' + result.id +  " ExpiredAt: " + result.get("expiredAt") + " - " + dtExpiredAt);
            }
            result.set("expiredAt", dtExpiredAt);
            clazzArray.push(result);
        })

        if (Constants.DEBUG) {
            console.log('>>> clazzArray:' + clazzArray.length);
        }
        promises.push(Parse.Object.saveAll(clazzArray, {
            success: function(objs) {
                console.log('>>> updateExpiredAt: success' + objs.length);
                return objs;
            },
            error: function(error) { 
                console.log('>>> updateExpiredAt: error' + error);
                throw error;
            }
        }));

        return Parse.Promise.when(updatePromises);
    });
}