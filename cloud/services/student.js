var _ = require('underscore');

var Class = Parse.Object.extend('Class');
var Teacher = Parse.Object.extend('_User');
var Student = Parse.Object.extend('Student');
var Constants = require('cloud/util/constants.js');

exports.add_student = function(name, phoneNumber, objectIdTeacher) {

	var teacher = new Teacher();
	teacher.id = objectIdTeacher;

	var student = new Student();
	student.set("phone", parseInt(phoneNumber));
	student.set("name", name);
	var relation = student.relation("instructors");
	relation.add(teacher);

	//verifica se já existe aluno com este telefone para o professor
 	var query = new Parse.Query(Student);
    query.equalTo("phone", parseInt(phoneNumber));
    query.limit(1);
  	query.containedIn("instructors", [teacher]);

	return query.find(function(results) {
    	if (Constants.DEBUG && results != null) {
			console.log('>>> add_student - nrStudents: ' + results.length);
    	}
        return results;
	}).then(function(results){
		if (results == null || results == undefined || results.length <= 0) {
			promises = [];

		    promises.push(student.save(null, {
		        success: function(objs) {
		            return objs;
		        },
		        error: function(error) { 
		            throw error;
		        }
		    }));

		    return Parse.Promise.when(promises);
		} else {
			return {'error':true, msg_error:'Aluno já cadsatrado com este telefone.'};
		}
    });	
};


var get_summary_teste = function(phoneNumber, objectIdTeacher) {
    if (Constants.DEBUG) {
		console.log('student_get_summary >>> ' + 'phoneNumber: ' + phoneNumber + ' objectIdTeacher: ' + objectIdTeacher);
	}

	var resultsJson = null;
	var type = null;
	var recentClasses = null;
	var qtClasses = 0;
	var resultDate = null;
	var studentName = null;

	var teacher = new Teacher();
	teacher.id = objectIdTeacher;

	var teacherQuery = new Parse.Query(Teacher);
	teacherQuery.equalTo("objectId", objectIdTeacher);

	var studentQuery = new Parse.Query(Student);
	studentQuery.equalTo("phone", parseInt(phoneNumber));

    var query = new Parse.Query(Class);
    query.select('objectId', 'done', 'usedAt', 'expiredAt', 'payAt', 'student');
    query.equalTo("done", true);
    query.exists("usedAt");
    query.limit(Constants.LIMIT_RECENT_CLASSES);
  	query.containedIn("instructor", [teacher]);
    query.matchesQuery("student", studentQuery);
    query.ascending("usedAt");

    return query.find(function(results) {
    	if (Constants.DEBUG && results != null) {
			console.log('>>> student_get_summary nr recent classes: ' + results.length);
    	}    	
    	recentClasses = results;
        return results;
	}).then(function(resultRecentClasses){
		//recupera última aula aberta para pegar o tipo e a data

		//query PAGA Antes
		var queryType1 = new Parse.Query(Class);
    	queryType1.equalTo("done", false);
    	queryType1.notEqualTo("paid", false);
		queryType1.containedIn("instructor", [teacher]);
	    queryType1.matchesQuery("student", studentQuery);
		queryType1.doesNotExist("usedAt");

		//query PAGA Depois
		var queryType2 = new Parse.Query(Class);
    	queryType2.equalTo("done", true);
    	queryType2.equalTo("paid", false);
	  	queryType2.containedIn("instructor", [teacher]);
	    queryType2.matchesQuery("student", studentQuery);
		queryType2.exists("usedAt");

		var mainQuery = Parse.Query.or(queryType1, queryType2);
		mainQuery.descending("updatedA,createdAt");
	    return mainQuery.first();
    }).then(function(result) {
    	if (Constants.DEBUG) {
			console.log('student_get_summary >>> result '  + result.get("objectId"));
		}
    	if (result == null || result == undefined) {
    		type = null;
    		resultDate = null;
    	} else {
    		if (Constants.DEBUG) {
    			console.log('student_get_summary >>> result '  + result.id + " - " + result.get("usedAt")+ " - " + result.get("expiredAt") + " - payAt: " + result.get("payAt"));
	    	}
	    	if (result.get("done") && !result.get("paid")) {
	    		type = Constants.TYPE_PAY_AFTER;
	    		resultDate = result.get("payAt");
	    	} else {
	    		type = Constants.TYPE_PAY_BEFORE;
	    		resultDate = result.get("expiredAt");
	    	}
    	}
    	return result;
    }).then(function(results){
    	if (type == null) {
    		return 0;
    	} else {
	    	var queryCount = new Parse.Query(Class);
	    	if (type == Constants.TYPE_PAY_BEFORE) {
			    var campareDate = new Date();
			    campareDate.setMinutes(0);
			    campareDate.setHours(0);
			    campareDate.setSeconds(0);
			    campareDate.setMilliseconds(0);
    			console.log(">>> campareDate: " + campareDate);
				queryCount.equalTo("done", false);
				queryCount.notEqualTo("paid", false);
				queryCount.greaterThanOrEqualTo( "expiredAt", campareDate);
				queryCount.doesNotExist("usedAt");
	    	} else {
	    		queryCount.equalTo("done", true);
	    		queryCount.equalTo("paid", false);
				queryCount.exists("usedAt");
	    	}
	    	
		    queryCount.exists("student");
		  	queryCount.containedIn("instructor", [teacher]);
		    queryCount.matchesQuery("student", studentQuery);
		    return queryCount.count();
		}
    }).then(function(resultQtClasses) {
    	qtClasses = resultQtClasses;
    	
    	var studentNameQuery = new Parse.Query(Student);
	    studentNameQuery.select("name");
	    studentNameQuery.containedIn("instructors", [teacher]);
    	studentNameQuery.equalTo("phone", parseInt(phoneNumber));
	    return studentNameQuery.first();
	}).then(function(studentNameResult){
		studnetName = "";
		if (studentNameResult != null && studentNameResult != undefined) {
			studnetName = studentNameResult.get("name");
		}
    	resultsJson = {'student_name':studnetName, 'qt_classes':qtClasses, 'type':type, 'recent_classes': recentClasses, 'date': resultDate}
        return resultsJson;
    });
};

exports.get_summary_teste = get_summary_teste;

var get_summary = function(phoneNumber, objectIdTeacher) {
    if (Constants.DEBUG) {
		console.log('student_get_summary >>> ' + 'phoneNumber: ' + phoneNumber + ' objectIdTeacher: ' + objectIdTeacher);
	}

	var resultsJson = null;
	var type = null;
	var recentClasses = null;
	var qtClasses = 0;
	var resultDate = null;
	var studentName = null;

	var teacher = new Teacher();
	teacher.id = objectIdTeacher;

	var teacherQuery = new Parse.Query(Teacher);
	teacherQuery.equalTo("objectId", objectIdTeacher);

	var studentQuery = new Parse.Query(Student);
	studentQuery.equalTo("phone", parseInt(phoneNumber));

    var query = new Parse.Query(Class);
    query.select('objectId', 'done', 'usedAt', 'expiredAt', 'payAt', 'student');
    query.equalTo("done", true);
    query.exists("usedAt");
    query.limit(Constants.LIMIT_RECENT_CLASSES);
  	query.containedIn("instructor", [teacher]);
    query.matchesQuery("student", studentQuery);
    query.ascending("usedAt");

    return query.find(function(results) {
    	if (Constants.DEBUG && results != null) {
			console.log('>>> student_get_summary nr recent classes: ' + results.length);
    	}    	
    	recentClasses = results;
        return results;
	}).then(function(resultRecentClasses){
		//recupera última aula aberta para pegar o tipo e a data

		//query PAGA Antes
		var queryType1 = new Parse.Query(Class);
    	queryType1.equalTo("done", false);
    	queryType1.notEqualTo("paid", false);
		queryType1.containedIn("instructor", [teacher]);
	    queryType1.matchesQuery("student", studentQuery);
		queryType1.doesNotExist("usedAt");

		//query PAGA Depois
		var queryType2 = new Parse.Query(Class);
    	queryType2.equalTo("done", true);
    	queryType2.equalTo("paid", false);
	  	queryType2.containedIn("instructor", [teacher]);
	    queryType2.matchesQuery("student", studentQuery);
		queryType2.exists("usedAt");

		var mainQuery = Parse.Query.or(queryType1, queryType2);
		mainQuery.descending("updatedA,createdAt");
	    return mainQuery.first();
    }).then(function(result) {
    	if (Constants.DEBUG) {
			console.log('student_get_summary >>> result '  + result);
		}
    	if (result == null || result == undefined) {
    		type = null;
    		resultDate = null;
    	} else {
    		if (Constants.DEBUG) {
    			console.log('student_get_summary >>> result '  + result.id + " - " + result.get("usedAt")+ " - " + result.get("expiredAt") + " - payAt: " + result.get("payAt"));
	    	}
	    	if (result.get("done") && !result.get("paid")) {
	    		type = Constants.TYPE_PAY_AFTER;
	    		resultDate = result.get("payAt");
	    	} else {
	    		type = Constants.TYPE_PAY_BEFORE;
	    		resultDate = result.get("expiredAt");
	    	}
    	}
    	return result;
    }).then(function(results){
    	if (type == null) {
    		return 0;
    	} else {
	    	var queryCount = new Parse.Query(Class);
	    	if (type == Constants.TYPE_PAY_BEFORE) {
			    var campareDate = new Date();
			    campareDate.setMinutes(0);
			    campareDate.setHours(0);
			    campareDate.setSeconds(0);
			    campareDate.setMilliseconds(0);
    			console.log(">>> campareDate: " + campareDate);
				queryCount.equalTo("done", false);
				queryCount.notEqualTo("paid", false);
				queryCount.greaterThanOrEqualTo( "expiredAt", campareDate);
				queryCount.doesNotExist("usedAt");
	    	} else {
	    		queryCount.equalTo("done", true);
	    		queryCount.equalTo("paid", false);
				queryCount.exists("usedAt");
	    	}
	    	
		    queryCount.exists("student");
		  	queryCount.containedIn("instructor", [teacher]);
		    queryCount.matchesQuery("student", studentQuery);
		    return queryCount.count();
		}
    }).then(function(resultQtClasses) {
    	qtClasses = resultQtClasses;
    	
    	var studentNameQuery = new Parse.Query(Student);
	    studentNameQuery.select("name");
	    studentNameQuery.containedIn("instructors", [teacher]);
    	studentNameQuery.equalTo("phone", parseInt(phoneNumber));
	    return studentNameQuery.first();
	}).then(function(studentNameResult){
		studnetName = "";
		if (studentNameResult != null && studentNameResult != undefined) {
			studnetName = studentNameResult.get("name");
		}
    	resultsJson = {'student_name':studnetName, 'qt_classes':qtClasses, 'type':type, 'recent_classes': recentClasses, 'date': resultDate}
        return resultsJson;
    });
};

exports.get_summary = get_summary;

exports.get_teachers = function(phone) {

	console.log('Constants.DEBUG ' + Constants.DEBUG);
	if (Constants.DEBUG) {
		console.log('get_teachers >>> ' + 'phone: ' + parseInt(phone));
	}

    var query = new Parse.Query(Student);
    query.equalTo("phone", parseInt(phone));

    var objectIdStudent = null;

    var resultTeachers = null;
    var students = [];
    return query.find(function(results) {
    	if (Constants.DEBUG && results != null && results != undefined) {
    		console.log('get_teachers >>> ' + 'instructors: ' + results.length);
    	}
        return results;
    }).then(function(results){
    	students = results;
    	if (results != null && results != undefined && results.length > 0) {
    		var studentsToSave = [];
    		_.each(results, function(student) {  
    			if (!student.get("login")) {
					student.set("login", true);
    				studentsToSave.push(student);
    			}
	        });
	        if (studentsToSave != null && studentsToSave != undefined && studentsToSave.length > 0) {
	        	promisesToSave = [];
	        	promisesToSave.push(Parse.Object.saveAll(studentsToSave, {
		            success: function(objs) {
		                return results;
		            },
		            error: function(error) { 
		                throw error;
		            }
		        }));

		        return Parse.Promise.when(promisesToSave);
	        }
    	}
    	return results;
    }).then(function(results){
    	if (students != null && students != undefined && students.length > 0) {
			queryInstructors = []
	        _.each(students, function(result) {            
	        	var relationQuery = result.relation("instructors").query();
	            queryInstructors.push(relationQuery);
	        })
	    
	    	objectIdStudent = students[0].id;

	    	if (Constants.DEBUG) {
				console.log('get_teachers >>> ' + 'objectIdStudent: ' + objectIdStudent);
			}

	    	var mainQuery = Parse.Query.or.apply(Parse.Query, queryInstructors);
	 		mainQuery.ascending("name");
	 		mainQuery.select('name', 'objectId')
	 		if (Constants.DEBUG) {
				console.log('get_teachers >>> mainQuery.find()');
			}
		    return mainQuery.find();
    	}
    	return [];		
	}).then(function(teachers){
		resultTeachers = teachers;
		if (Constants.DEBUG) {
			console.log('get_teachers >>> return instructors');
		}
		if (resultTeachers != null && resultTeachers.length == 1) {
			if (Constants.DEBUG) {
				console.log('student_get >>> ' + 'objectIdStudent: ' + objectIdStudent + ' teacherId: ' + resultTeachers[0].id);
			}
			return get_summary(phone, resultTeachers[0].id);
		} else {
			return null;
		}
	}).then(function(summary) {
		if (summary != null && resultTeachers != null && resultTeachers.length == 1) {
			var result = [];
			result[0] = resultTeachers[0].toJSON();
			result[0].summary = summary;
			//return {'nr_classes':resultTeachers, 'summary':summary}
			//resultTeachers[0].set("summary", summary);
			//resultTeachers[0].set("name", "xxxx");
			return result;
		}

		return resultTeachers;
    });

};