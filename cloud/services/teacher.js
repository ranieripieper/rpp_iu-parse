var _ = require('underscore');

var Class = Parse.Object.extend('Class');
var Teacher = Parse.Object.extend('_User');
var Student = Parse.Object.extend('Student');
var Constants = require('cloud/util/constants.js');


exports.get_students = function(email) {
    //if (Constants.DEBUG) {
		console.log('get_students >>> ' + 'email: ' + email);
	//}

	var teacherQuery = new Parse.Query(Teacher);
	teacherQuery.equalTo("email", email);

    var query = new Parse.Query(Student);
    query.select("objectId", "name", "phone", "createdAt");
  	query.matchesQuery("instructors", teacherQuery);
    query.ascending("name");

    return query.find(function(results) {
        return results;
	});
};

