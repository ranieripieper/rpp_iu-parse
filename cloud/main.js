var _ = require('underscore');

var Class = Parse.Object.extend('Class');
var Teacher = Parse.Object.extend('_User');
var Student = Parse.Object.extend('Student');
var Constants = require('cloud/util/constants.js');
var studentService = require('cloud/services/student.js');
var classesService = require('cloud/services/classes.js');
var teacherService = require('cloud/services/teacher.js');
var emailUtil = require('cloud/util/email.js');

//jobs
Parse.Cloud.job("SendMail", function(request, status) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey();

    var query = new Parse.Query(Teacher);
    query.notEqualTo("sentWelcomeMail", true);

    return query.find(function(results) {
        if (Constants.DEBUG && results != null) {
            console.log('>>> SendMail nr teachers: ' + results.length);
        } 
        return results;
    }).then(function(teachers){
        if (teachers != null && teachers.length > 0) {
            promises = [];
            _.each(teachers, function(teacher) {
                console.log('>>> teacher_id: ' + teacher.id);
                promises.push(emailUtil.send_welcome_teacher(teacher));
            });
            return Parse.Promise.when(promises);
        } else {
            return teachers;
        }

    }).then(function(teachers){
        status.success("Emails sent ");        
    }, function(error) {
        status.error({'error':true, 'msg_error': error});
    });
});

//functions
Parse.Cloud.define("student_get_summary", function(request, response) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey();

    var phoneNumberStudent = request.params.phone_number;
    var objectIdTeacher = request.params.teacher_id;

    studentService.get_summary(phoneNumberStudent, objectIdTeacher).then(function(result){
        response.success(result);
    }, function(error) {
        response.error({'error':true, 'msg_error': error});
    });
});

Parse.Cloud.define("student_get_summary_teste", function(request, response) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey();

    var phoneNumberStudent = request.params.phone_number;
    var objectIdTeacher = request.params.teacher_id;

    studentService.get_summary_teste(phoneNumberStudent, objectIdTeacher).then(function(result){
        response.success(result);
    }, function(error) {
        response.error({'error':true, 'msg_error': error});
    });
});

Parse.Cloud.define("student_get_teachers", function(request, response) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey();

    var phone = request.params.phone_number;

    studentService.get_teachers(phone).then(function(result){
        console.log('student_get_teachers >>> result ' + Math.random());
        response.success(result);
    }, function(error) {
        console.log('student_get_teachers >>> error');
        response.error({'error':true, 'msg_error': error});
    });
});

Parse.Cloud.define("classes_get_done", function(request, response) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey();

    var phoneNumber = request.params.phone_number;
    var objectIdTeacher = request.params.teacher_id;
    var page = request.params.page;

    classesService.get_done(phoneNumber, objectIdTeacher, page).then(function(result){
        response.success(result);
    }, function(error) {
        response.error({'error':true, 'msg_error': error});
    });
});

Parse.Cloud.define("classes_pay_before_add_classes", function(request, response) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey();

    var qtClasses = request.params.qt_classes;
    var objectIdTeacher = request.params.teacher_id;
    var objectIdStudent = request.params.student_id;
    var dtValidade = request.params.dt_validade;

    classesService.pay_before_add_classes(objectIdStudent, objectIdTeacher, qtClasses, dtValidade).then(function(result){
        response.success({'success':true});
    }, function(error) {
        response.error({'error':true, 'msg_error': error});
    });
});

Parse.Cloud.define("classes_pay_before_set_done_classes", function(request, response) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey();

    var objectIdTeacher = request.params.teacher_id;
    var objectIdStudent = request.params.student_id;
    var expiredAt = request.params.expired_at;
    var dates = request.params.dates;

    classesService.pay_before_set_done(objectIdStudent, objectIdTeacher, expiredAt, dates).then(function(result){
        response.success({'success':true});
    }, function(error) {
        response.error({'error':true, 'msg_error': error});
    });

});

Parse.Cloud.define("classes_pay_after_add_classes", function(request, response) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey();

    var objectIdTeacher = request.params.teacher_id;
    var objectIdStudent = request.params.student_id;
    var dtPayment = request.params.pay_at;
    var dates = request.params.dates;

    classesService.pay_after_add_classes(objectIdStudent, objectIdTeacher, dtPayment, dates).then(function(result){
        response.success({'success':true});
    }, function(error) {
        response.error({'error':true, 'msg_error': error});
    });

});

Parse.Cloud.define("classes_pay_after_update_to_paid", function(request, response) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey();

    var objectIdTeacher = request.params.teacher_id;
    var objectIdStudent = request.params.student_id;
    var qtClasses = request.params.qt_classes;
    var dtPayment = request.params.pay_at;

    classesService.pay_after_update_to_paid(objectIdStudent, objectIdTeacher, qtClasses, dtPayment).then(function(result){
        response.success({'success':true});
    }, function(error) {
        response.error({'error':true, 'msg_error': error});
    });

});

Parse.Cloud.define("classes_pay_after_update_dt_pay", function(request, response) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey();

    var objectIdTeacher = request.params.teacher_id;
    var objectIdStudent = request.params.student_id;
    var dtPayment = request.params.pay_at;

    classesService.pay_after_update_dt_pay(objectIdStudent, objectIdTeacher, dtPayment).then(function(result){
        response.success({'success':true});
    }, function(error) {
        response.error({'error':true, 'msg_error': error});
    });

});

Parse.Cloud.define("teachers_get_students", function(request, response) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey();

    var emailTeacher = request.params.email;

    teacherService.get_students(emailTeacher).then(function(results){
        response.success(results);
    }, function(error) {
        response.error({'error':true, 'msg_error': error});
    });

});

Parse.Cloud.define("students_add", function(request, response) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey();

    var objectIdTeacher = request.params.teacher_id;
    var studentName = request.params.student_name;
    var studentPhone = request.params.student_phone;

    if (Constants.DEBUG) {
        console.log('students_add >>> - objectIdTeacher:' + objectIdTeacher + " studentName: " + studentName + " studentPhone: " + studentPhone);
    }

    //validate name
    if (studentName == null || studentName == undefined || studentName.length < 3) {
        var resultsJson = {'error':true, 'msg_error': 'Nome inválido.'}
        response.success(resultsJson);
    } else if (studentPhone == null || studentPhone == undefined || studentPhone.length < 10) {
        response.success({'error':true, 'msg_error': 'Telefone inválido.'});
    } else if (objectIdTeacher == null || objectIdTeacher == undefined) {
        response.success({'error':true, 'msg_error': 'Professor inválido.'});
    } else {
        studentService.add_student(studentName, studentPhone, objectIdTeacher).then(function(results){
            response.success(results);
        }, function(error) {
            response.error({'error':true, 'msg_error': error});
        });
    }

});

/*
Parse.Cloud.define("update_classes_paid", function(request, response) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey();

    var query = new Parse.Query(Class);
    query.equalTo("paid", false);
    query.find(function(results) {
        return results;
    }).then(function(results) {
        promises = []
        _.each(results, function(result) {  
            result.set("paid", true);          
            promises.push(result.save());
         })

        console.log('student_get_teachers >>> result ' + Math.random());
        return Parse.Promise.when(promises);
    }).then(function(result){
        response.success(result);
    }, function(error) {
        console.log('student_get_teachers >>> error');
        response.error({'error':true, 'msg_error': error});
    });
});
*/

Parse.Cloud.afterDelete("Student", function(request) {
    if (Constants.DEBUG) {
        console.log('Deleting Classes >>> - StudentId:' + request.object.id);
    }
    var student = new Student();
    student.id = request.object.id;

    var query = new Parse.Query(Class);
    query.equalTo("student", student);
    
    query.find().then(function(classes) {
        if (classes != null && classes != undefined) {
            if (Constants.DEBUG) {
                console.log('Deleting Class >>> - StudentId:' + request.object.id + " - QtdeClasses: " + classes.length);
            }
            return Parse.Object.destroyAll(classes);
        } else {
            if (Constants.DEBUG) {
                console.log('Deleting Class >>> - StudentId:' + request.object.id + " - QtdeClasses: 0");
            }
            return classes;
        }
        
    }).then(function(success) {
        // The related comments were deleted
    }, function(error) {
        console.error("Error deleting related Class - StudentId:" + request.object.id + " Error: "+ error.code + ": " + error.message);
    });
});